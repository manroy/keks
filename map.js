'use strict';

var TITLES = [
  'Большая уютная квартира',
  'Маленькая неуютная квартира',
  'Огромный прекрасный дворец',
  'Маленький ужасный дворец',
  'Красивый гостевой домик',
  'Некрасивый негостеприимный домик',
  'Уютное бунгало далеко от моря',
  'Неуютное бунгало по колено в воде'
];

var TYPES = ['flat', 'house', 'bungalo'];

var CHECKIN = ['12:00', '13:00', '14:00'];
var CHECKOUT = ['12:00', '13:00', '14:00'];

var FEATURES = ['wifi', 'dishwasher', 'parking', 'washer', 'elevator', 'conditioner'];

var PHOTOS = ['http://o0.github.io/assets/images/tokyo/hotel1.jpg', 'http://o0.github.io/assets/images/tokyo/hotel2.jpg', 'http://o0.github.io/assets/images/tokyo/hotel3.jpg'];

var ROOMS_MIN = 1;
var ROOMS_MAX = 5;

var PRICE_MIN = 1000;
var PRICE_MAX = 1000000;

var LOCATION_X_MIN = 300;
var LOCATION_X_MAX = 900;

var LOCATION_Y_MIN = 150;
var LOCATION_Y_MAX = 500;

var amount = 8;

function getRandomData(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var locationX = getRandomData(LOCATION_X_MIN, LOCATION_X_MAX);
var locationY = getRandomData(LOCATION_Y_MIN, LOCATION_Y_MAX);

// Создаем массив из JS объекта
var getOffers = function () {
  var offersArr = [];
  for (var i = 1; i <= amount; i++) {
    var offerAd = {
      'author': {
        'avatar': 'img/avatars/user0' + i + '.png'
      },

      'offer': {
        'title': TITLES[i],
        'address': locationX + ', ' + locationY,
        'price': getRandomData(PRICE_MIN, PRICE_MAX),
        'type': TYPES[getRandomData(0, TYPES.length - 1)],
        'rooms': getRandomData(ROOMS_MIN, ROOMS_MAX),
        'guests': getRandomData(1, 10),
        'checkin': CHECKIN[getRandomData(0, CHECKIN.length - 1)],
        'checkout': CHECKOUT[getRandomData(0, CHECKOUT.length - 1)],
        'features': FEATURES[getRandomData(0, FEATURES.length - 1)],
        'description': {},
        'photos': PHOTOS[getRandomData(0, PHOTOS.length - 1)]
      },

      'location': {
        'x': locationX,
        'y': locationY
      }
    };
    offersArr.push(offerAd);
  }
  return offersArr;
};


// Активируем карту
var map = document.querySelector('.map');
map.classList.remove('map--faded');

// Элемент пина
var renderPin = function (data, template) {
 var element = template.cloneNode(true);
 element.style.left = data.location.x + 'px';
 element.style.top = data.location.y + 'px';
 element.querySelector('img').src = data.author.avatar;

 return element;
};

// Отрисовываем фичи
var renderOfferFeatures = function () {
  container = deleteNodeChildren(container);
  for (var i = 0; i < arr.length; i++) {
    var item = document.createElement('li');
    item.classList.add('feature', 'feature--' + arr[i]);
    container.appendChild(item);
  }
};

// Отрисоываем фото
var renderAdvertCardPhotos = function () {
  for (var i = 0; i < arr.length; i++) {
    var item = document.createElement('li');
    var image = document.createElement('img');
    image.src = arr[i];
    item.appendChild(image);
    container.appendChild(item);
  }
};

// Генерируем элементы объявлений
var renderOfferPins = function (arr) {
  var template = document.querySelector('template').content.querySelector('.map__pin');
  var fragment = document.createDocumentFragment();
  for (var i = 0; i < arr.length; i++) {
    var element = renderPin(arr[i], template);
    fragment.appendChild(element);
  }
  document.querySelector('.map__pins').appendChild(fragment);
};

var offerTemplate = document.querySelector('template').content.querySelector('article.map__card');

var renderOffer = function (data) {
  var offerElem = offerTemplate.cloneNode(true);

  offerElem.querySelector('h3').textContent = data.offer.title;
  offerElem.querySelector('small').textContent = data.offer.address;
  offerElem.querySelector('.popup__price').textContent = data.offer.price + ' ' + '₽' + '/ночь';
  offerElem.querySelector('h4').textContent = TYPES[data.offer.type];
  offerElem.querySelector('h4 + p').textContent = data.offer.rooms + ' комнаты для ' + data.offer.guests + ' гостей';
  offerElem.querySelector('h4 + p + p').textContent = 'Заезд после ' + data.offer.checkin + ', выезд до ' + data.offer.checkout;
  offerElem.querySelector('.popup__features + p').textContent = data.offer.description;
  offerElem.querySelector('.popup__avatar').src = data.author.avatar;

};

// Массив
var offersArr = getOffers(amount);

// Отрисовываем пины на карту
renderOfferPins(offersArr);

// Отрисовываем карточку первого элемента массива объявлений
renderOffer(offersArr[0]);

